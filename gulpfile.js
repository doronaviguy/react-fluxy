var browserify = require('browserify');
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var reactify = require('reactify');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var path = require('path');
var gutil = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');

var exorcist= require('exorcist');
var mapfile    = path.join(__dirname,'/assets/', 'bundle.js.map');





gulp.task('connect', function() {
  connect.server({
    root: 'assets',
    port: 8001,
    fallback: 'index.html',
  });
});



var path =  {
    scss: './scss/*',
    js: './js/*/*',
    scssOut: './css',
    partials: './public/templates/*.html'
};



var packageJson = require('./package.json');
var dependencies = Object.keys(packageJson && packageJson.dependencies || {});


gulp.task('browserify-libs', function () {
    console.dir(dependencies);
    gutil.log('process.env.NODE_ENV ', process.env.NODE_ENV);
    if(process.env.NODE_ENV != 'production') {
        gutil.log(' please set it to produciton', gutil.colors.red('Error'));
        gutil.beep();
        return;
    }
    return browserify()
    .require(dependencies)
    .bundle()
    .on('error', function(e) {console.log(e);})
    .pipe(source('libs.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./assets/'));
});



gulp.task('browserify', function(){
    var b = browserify({transform: reactify, debug: true});
    b.add('./js/main.js');
    b.external('react');
    b.external(dependencies);
    b.bundle()
    .on('error', function(err){
      // print the error (can replace with gulp-util)
      console.log(err.message);
      // end this stream
      this.end();
    })
    .pipe(exorcist(mapfile))
    .pipe(source('main.js'))
    .pipe(gulp.dest('./assets'));

    gulp.src('index.html')
        .pipe(gulp.dest('./assets'))

});



gulp.task('sass', function () {
    gulp.src(path.scss)
        .pipe(sass({errLogToConsole: true}))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist'))
});

gulp.task('watch', function() {

    gulp.watch('./*', ['fast-watch']);
    gulp.watch('./js/*', ['fast-watch']);
    gulp.watch(path.js, ['fast-watch']);
    gulp.watch(path.scss, ['sass']);

});


gulp.task('fast-watch', function() {

    var bundler = watchify(browserify('./js/main.js',
    {
        transform: reactify,
        debug: true,
    }));

    bundler.external(dependencies);
    bundler.on('update', rebundle);

    function rebundle() {
        setTimeout(function() {
            gulp.run('deploy');
        }, 5000);
        return bundler.bundle()
            .on('error', function(e) {console.log('Browserify Error', e)})
            .pipe(exorcist(mapfile))
            .pipe(source('main.js'))
            .pipe(gulp.dest('./dist'));
    }

    return rebundle();


});

gulp.task('default', ['connect', 'watch']);